apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Release.Name }}-nginx-config
data:
  nginx.conf: |
    worker_processes auto;

    events {
      worker_connections 1024;
    }

    # see "Running nginx as a non-root user" in https://hub.docker.com/_/nginx/
    pid /tmp/nginx.pid;

    http {
      # More non-root stuff
      client_body_temp_path /tmp/client_temp;
      proxy_temp_path       /tmp/proxy_temp_path;
      fastcgi_temp_path     /tmp/fastcgi_temp;
      uwsgi_temp_path       /tmp/uwsgi_temp;
      scgi_temp_path        /tmp/scgi_temp;

      server {
        listen 8443 ssl;

        ssl_certificate        /etc/nginx/tls/server/tls.crt;
        ssl_certificate_key    /etc/nginx/tls/server/tls.key;
        ssl_client_certificate /etc/nginx/client_validation_certs/ca.crt;
        ssl_verify_client      optional;
        ssl_protocols          TLSv1.2 TLSv1.3;
        ssl_ciphers            HIGH:!aNULL:!MD5;

        # For details see here: https://wikitech.wikimedia.org/wiki/Wikimedia_Cloud_Services_team/EnhancementProposals/Toolforge_API_gateway#Authentication
        proxy_ssl_certificate         /etc/nginx/tls/backend/tls.crt;
        proxy_ssl_certificate_key     /etc/nginx/tls/backend/tls.key;
        proxy_ssl_verify              on;
        proxy_ssl_trusted_certificate /etc/nginx/tls/backend/ca.crt;
        proxy_set_header              ssl-client-subject-dn $ssl_client_s_dn;

        location = / {
          return 200 "This is the Toolforge API gateway!\n";
        }

        location /openapi.json {
          proxy_pass http://127.0.0.1:8000/openapi.json;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header ssl-client-subject-dn $ssl_client_s_dn;
        }

        location /auth {
          proxy_pass http://127.0.0.1:8000/auth;
          proxy_pass_request_body off;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Content-Length "";
          proxy_set_header X-Original-URI $request_uri;
          proxy_set_header ssl-client-subject-dn $ssl_client_s_dn;
        }

        location /healthz {
          proxy_pass http://127.0.0.1:8000/healthz;
          proxy_pass_request_body off;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header Content-Length "";
        }

        # the resolver entry is mandatory if using the per-location resolution
        resolver kube-dns.kube-system.svc.{{ .Values.certificates.internalClusterDomain }};

{{- $internalClusterDomain := .Values.certificates.internalClusterDomain -}}
{{ range .Values.backends }}
        location {{ .path }}/ {
          auth_request /auth;

          # needed to extract the value from the headers to the auth request url
          auth_request_set $tool $upstream_http_x_toolforge_tool;
          auth_request_set $user $upstream_http_x_toolforge_user;
          # this will overwrite the header even if already set by the client
          proxy_set_header X-Toolforge-Tool $tool;
          proxy_set_header X-Toolforge-User $user;
          # for some reason we have to set this again
          proxy_set_header ssl-client-subject-dn $ssl_client_s_dn;
          # Avoid resolving outside location, so if it can't resolve, it will not fail to start completely
          # need the .cluster.local as it will not add them as it uses a custom resolver entry
          set $upstream {{ .target.service }}.{{ .target.namespace }}.svc.{{ $internalClusterDomain }};
          # The rewrite has to happen after the set
          rewrite {{.path}}/(.*) /$1 break;
          proxy_pass "https://$upstream:{{ .target.port }}";
        }

        location ~ {{ .path }}/.*/logs {
          auth_request /auth;

          # needed to extract the value from the headers to the auth request url
          auth_request_set $tool $upstream_http_x_toolforge_tool;
          auth_request_set $user $upstream_http_x_toolforge_user;
          # this will overwrite the header even if already set by the client
          proxy_set_header X-Toolforge-Tool $tool;
          proxy_set_header X-Toolforge-User $user;
          # for some reason we have to set this again
          proxy_set_header ssl-client-subject-dn $ssl_client_s_dn;
          set $upstream {{ .target.service }}.{{ .target.namespace }}.svc.{{ $internalClusterDomain }};
          rewrite {{.path}}/(.*) /$1 break;
          proxy_read_timeout 10m;
          proxy_pass https://$upstream:{{ .target.port }};
        }
{{ end }}
      }

      server {
        listen 9000;
        access_log off;

        location = /healthz {
          return 200 "ok";
        }

        location /app_healthz {
          proxy_pass http://127.0.0.1:8000/healthz;
          proxy_set_header Host $host;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Proto $scheme;
        }
      }
    }
