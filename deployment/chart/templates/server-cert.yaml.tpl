apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: {{ .Release.Name }}-server
spec:
  commonName: {{ .Release.Name }}.{{ .Release.Namespace }}.svc
  dnsNames:
    - {{ .Release.Name }}.{{ .Release.Namespace }}.svc
{{- range .Values.certificates.extraNames }}
    - {{.}}
{{- end }}
  secretName: {{ .Release.Name }}-server
  usages:
    - server auth
  duration: "504h" # 21d
  privateKey:
    algorithm: ECDSA
    size: 256
  issuerRef:
    name: {{ .Release.Name }}-server
    kind: Issuer
    group: cert-manager.io
