apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-nginx
  labels:
    name: {{ .Release.Name }}-nginx
  annotations:
    # use https://github.com/stakater/reloader (via the cert-manager deployment)
    # to restart when certificates get renewed or config gets updated
    configmap.reloader.stakater.com/reload: "{{ .Release.Name }}-nginx-config"
    secret.reloader.stakater.com/reload: "{{ .Release.Name }}-server,{{ .Release.Name }}-backend-server"
spec:
  replicas: {{ .Values.nginx.replicas }}
  selector:
    matchLabels:
      name: {{ .Release.Name }}-nginx
  template:
    metadata:
      labels:
        name: {{ .Release.Name }}-nginx
    spec:
      serviceAccountName: {{ .Release.Name }}-nginx
      containers:
        - name: app-server
          image: {{ .Values.app.image.name }}:{{ .Values.app.image.tag }}
          imagePullPolicy: {{ .Values.app.image.pullPolicy }}
          env:
            - name: "APP_LOG_LEVEL"
              value: "{{ .Values.app.log_level }}"
            - name: "APP_PORT"
              value: "{{ .Values.app.port }}"
            - name: "APP_ADDRESS"
              value: "{{ .Values.app.address }}"
            - name: "APP_CLIENT_CA_PATH"
              value: "{{ .Values.app.client_ca_path }}"
            - name: "APP_CLIENT_CERT_PATH"
              value: "{{ .Values.app.client_cert_path }}"
            - name: "APP_CLIENT_KEY_PATH"
              value: "{{ .Values.app.client_key_path }}"
            - name: "APP_BACKEND_TIMEOUT_S"
              value: "{{ .Values.app.backend_timeout_s }}"
            - name: "APP_BACKEND_LOGS_TIMEOUT_S"
              value: "{{ .Values.app.backend_logs_timeout_s }}"
            - name: "APP_API_MAPS"
              value: >
                {{ .Values.app.api_maps | toJson }}
            - name: "APP_OPENAPI_DOCS"
              value: >
                {{ .Values.app.openapi_docs | toJson }}
            - name: "APP_SUPERUSERS"
              value: >
                {{ .Values.app.superusers | toJson }}
          ports:
            - containerPort: {{ .Values.app.port }}
              name: http
              protocol: TCP
          startupProbe:
            httpGet:
              path: /app_healthz
              port: 9000
            # start asap
            initialDelaySeconds: 0
            # query often
            periodSeconds: 1
            # try 10 times
            failureThreshold: 10
            # switch to liveness probe as soon as it works once
            successThreshold: 1
          livenessProbe:
            httpGet:
              path: /app_healthz
              port: 9000
            # poll less frequently
            periodSeconds: 10
            # retry 3 time before failing
            failureThreshold: 3
            successThreshold: 1
          volumeMounts:
            - mountPath: /etc/nginx/tls/server
              name: server-certs
              readOnly: true
            - mountPath: /etc/nginx/tls/backend
              name: backend-certs
              readOnly: true
        - name: nginx
          image: {{ .Values.nginx.image.repository }}:{{ .Values.nginx.image.nginxTag }}
          imagePullPolicy: {{ .Values.nginx.image.pullPolicy }}
          ports:
            - containerPort: 8443
              name: https
              protocol: TCP
          volumeMounts:
            - mountPath: /etc/nginx/client_validation_certs
              name: client-validation-certs
              readOnly: true
            - mountPath: /etc/nginx/nginx.conf
              name: nginx-config
              readOnly: true
              subPath: nginx.conf
            - mountPath: /etc/nginx/tls/server
              name: server-certs
              readOnly: true
            - mountPath: /etc/nginx/tls/backend
              name: backend-certs
              readOnly: true
          securityContext:
            allowPrivilegeEscalation: false
            runAsUser: 101
            runAsGroup: 101
          livenessProbe:
            httpGet:
              path: /healthz
              port: 9000
            initialDelaySeconds: 3
            periodSeconds: 3
      initContainers:
        # in order to be able to validate both k8s ca signed certs (for users) and api-gateway-backend-ca signed certs
        # (for services) we need to bundle the ca certs
        - name: bundle-client-validation-certs
          image: {{ .Values.nginx.image.repository }}:{{ .Values.nginx.image.nginxTag }}
          imagePullPolicy: {{ .Values.nginx.image.pullPolicy }}
          command:
            - bash
            - -c
            - echo "Starting cert bundling";
              cat /etc/nginx/tls/backend/ca.crt /var/run/secrets/kubernetes.io/serviceaccount/ca.crt > /etc/nginx/client_validation_certs/ca.crt &&
              echo "Created cert bundle at /etc/nginx/client_validation_certs/ca.crt"
          volumeMounts:
            - mountPath: /etc/nginx/client_validation_certs
              name: client-validation-certs
              readOnly: false
            - mountPath: /etc/nginx/tls/backend
              name: backend-certs
              readOnly: true
          securityContext:
            allowPrivilegeEscalation: false
            runAsUser: 101
            runAsGroup: 101
      volumes:
        - name: client-validation-certs
          emptyDir: {}
        - name: server-certs
          secret:
            secretName: {{ .Release.Name }}-server
        - name: backend-certs
          secret:
            secretName: {{ .Release.Name }}-backend-server
        - configMap:
            items:
            - key: nginx.conf
              path: nginx.conf
            name: {{ .Release.Name }}-nginx-config
          name: nginx-config
      tolerations: {{ .Values.nginx.tolerations | toYaml | nindent 8 }}
      affinity: {{ .Values.nginx.affinity | toYaml | nindent 8 }}
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: ScheduleAnyway
          labelSelector:
            matchLabels:
              name: {{ .Release.Name }}-nginx
