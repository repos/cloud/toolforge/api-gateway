apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}
spec:
  selector:
    name: {{ .Release.Name }}-nginx
  type: {{ .Values.service.type }}
  ports:
    - name: https
{{ if .Values.service.nodePort }}
      nodePort: {{ .Values.service.nodePort }}
{{ end }}
      port: 443
      targetPort: 8443
      protocol: TCP
