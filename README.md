# Toolforge API gateway

The Toolforge API gateway service is a proxy that deals with access to
API services hosted in the Toolforge Kubernetes cluster.

## Configuring
By default, the api-gateway will not have any backends enabled, as it would fail to start if the backends are not there. If/when you install any backends (jobs-api, builds-api, ...) you can add them by editing the `local.yaml` file and following the examples there.

## Using with minikube
If you use minikube, you can deploy the gateway with the regular deploy script:
```
> ./deploy.sh local
```

Then you can open access to the service with:
```
> minikube service -n api-gateway api-gateway
```

That will show/open a url to that service, after that, you can curl it with (`192.168.49.2:30003` is the ip/port that the previous command gave):
```
> curl --cert ~/.minikube/profiles/minikube/client.crt --key ~/.minikube/profiles/minikube/client.key --insecure 'https://192.168.49.2:30003'
This is the Toolforge API gateway!
```

## Copyright and license
Copyright (c) 2023      Taavi Väänänen <hi@taavi.wtf>  
Copyright (c) 2023-2024 Wikimedia Foundation, Inc.

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program. If not, see
<https://www.gnu.org/licenses/>
