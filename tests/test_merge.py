from typing import Any

from app.merge import merge_specs


def get_fake_base_spec(**overrides: Any) -> dict[str, Any]:
    base_spec: dict[str, Any] = {
        "components": {},
    }
    base_spec.update(overrides)
    return base_spec


class TestMergeSpecs:
    def test_with_empty_specs(self) -> None:
        expected_spec = get_fake_base_spec(somekey="somevalue")
        final_spec = merge_specs(base_spec=expected_spec, specs={})

        assert final_spec == expected_spec

    def test_it_does_not_delete_base_paths(self) -> None:
        expected_spec = get_fake_base_spec(paths={"somekey": "somevalue"})
        final_spec = merge_specs(base_spec=expected_spec, specs={})

        assert final_spec == expected_spec

    def test_it_adds_one_spec_with_prefixed_paths(self) -> None:
        base_spec = get_fake_base_spec(paths={"/frombase": {"somekey": "somevalue"}})
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {"paths": {"/somepath": {}}}
        expected_paths = {
            "/frombase": {"somekey": "somevalue"},
            f"/{extra_spec_name}/somepath": {},
        }

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert final_spec["paths"] == expected_paths

    def test_it_adds_one_spec_with_prefixed_response_schemas(self) -> None:
        base_spec = get_fake_base_spec(paths={"/frombase": {"somekey": "somevalue"}})
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {},
            "components": {"responses": {"SomeData": {}}},
        }
        expected_responses: dict[str, Any] = {f"{extra_spec_name}SomeData": {}}

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert final_spec["components"]["responses"] == expected_responses

    def test_it_adds_one_spec_with_prefixed_responses_refs(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {
                "/somepath": {
                    "get": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/responses/buildsGetResponse"
                                }
                            }
                        }
                    }
                }
            },
        }
        expected_ref = f"#/components/responses/{extra_spec_name}buildsGetResponse"

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["content"][
                "application/json"
            ]["schema"]["$ref"]
            == expected_ref
        )

    def test_it_adds_one_spec_with_prefixed_schemas(self) -> None:
        base_spec = get_fake_base_spec(paths={"/frombase": {"somekey": "somevalue"}})
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {},
            "components": {"schemas": {"SomeData": {}}},
        }
        expected_schemas: dict[str, Any] = {f"{extra_spec_name}SomeData": {}}

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert final_spec["components"]["schemas"] == expected_schemas

    def test_it_adds_one_spec_with_prefixed_refs(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {
                "/somepath": {
                    "get": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/buildsGetResponse"
                                }
                            }
                        }
                    }
                }
            },
        }
        expected_ref = f"#/components/schemas/{extra_spec_name}buildsGetResponse"

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["content"][
                "application/json"
            ]["schema"]["$ref"]
            == expected_ref
        )

    def test_it_adds_one_spec_with_prefixed_refs_in_parameters_subpath(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {
                "/somepath": {
                    "get": {
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/parameters/buildsGetParam"
                                }
                            }
                        }
                    }
                }
            },
        }
        expected_ref = f"#/components/parameters/{extra_spec_name}buildsGetParam"

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["content"][
                "application/json"
            ]["schema"]["$ref"]
            == expected_ref
        )

    def test_it_adds_one_spec_with_prefixed_operation_ids(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {"/somepath": {"get": {"operationId": "someop"}}},
        }
        expected_operation_id = f"{extra_spec_name}_someop"

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["operationId"]
            == expected_operation_id
        )

    def test_it_adds_one_spec_with_prefixed_refs_in_discriminator_field(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        # extracted from jobs-api
        extra_spec: dict[str, Any] = {
            "paths": {},
            "components": {
                "schemas": {
                    "ScriptHealthCheck": {
                        "type": "object",
                        "properties": {
                            "type": "script",
                        },
                    },
                    "CommonJob": {
                        "properties": {
                            "health_check": {
                                "type": "object",
                                "oneOf": [
                                    {"$ref": "#/components/schemas/ScriptHealthCheck"}
                                ],
                                "discriminator": {
                                    "mapping": {
                                        "script": "#/components/schemas/ScriptHealthCheck"
                                    }
                                },
                                "propertyName": "type",
                            }
                        }
                    },
                }
            },
        }

        expected_ref = f"#/components/schemas/{extra_spec_name}ScriptHealthCheck"
        expected_discriminator = {
            "mapping": {
                "script": f"#/components/schemas/{extra_spec_name}ScriptHealthCheck"
            }
        }

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert (
            final_spec["components"]["schemas"][f"{extra_spec_name}CommonJob"][
                "properties"
            ]["health_check"]["oneOf"][0]["$ref"]
            == expected_ref
        )

        assert (
            final_spec["components"]["schemas"][f"{extra_spec_name}CommonJob"][
                "properties"
            ]["health_check"]["discriminator"]
            == expected_discriminator
        )

    def test_it_adds_tags(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {"/somepath": {"get": {"tags": ["ishouldberemoved"]}}},
            "tags": [{"name": "Ishouldberemoved"}],
        }
        expected_top_level_tags = [{"name": extra_spec_name.capitalize()}]
        expected_inner_tags = [extra_spec_name.capitalize()]

        final_spec = merge_specs(
            base_spec=base_spec, specs={extra_spec_name: extra_spec}
        )

        assert final_spec["tags"] == expected_top_level_tags
        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["tags"]
            == expected_inner_tags
        )

    def test_it_does_not_duplicate_tags(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec_name = "extra"
        extra_spec: dict[str, Any] = {
            "paths": {"/somepath": {"get": {"tags": ["ishouldberemoved"]}}},
            "tags": [{"name": "Ishouldberemoved"}],
        }
        expected_top_level_tags = [{"name": extra_spec_name.capitalize()}]
        expected_inner_tags = [extra_spec_name.capitalize()]

        final_spec = merge_specs(
            base_spec=base_spec,
            specs={
                extra_spec_name: extra_spec,
                extra_spec_name.capitalize(): extra_spec,
            },
        )

        assert final_spec["tags"] == expected_top_level_tags
        assert (
            final_spec["paths"][f"/{extra_spec_name}/somepath"]["get"]["tags"]
            == expected_inner_tags
        )

    def test_it_bumps_minor_version(self) -> None:
        base_spec = get_fake_base_spec(info={"version": "0.0.1"})
        extra_spec: dict[str, Any] = {
            "info": {"version": "0.0.1"},
            "paths": {},
            "tags": [],
        }
        expected_version = "0.0.2"

        final_spec = merge_specs(
            base_spec=base_spec,
            specs={
                "spec1": extra_spec,
            },
        )

        assert final_spec["info"]["version"] == expected_version

    def test_it_bumps_middle_version(self) -> None:
        base_spec = get_fake_base_spec(info={"version": "0.0.1"})
        extra_spec: dict[str, Any] = {
            "info": {"version": "0.4.1"},
            "paths": {},
            "tags": [],
        }
        expected_version = "0.4.2"

        final_spec = merge_specs(
            base_spec=base_spec,
            specs={
                "spec1": extra_spec,
            },
        )

        assert final_spec["info"]["version"] == expected_version

    def test_it_bumps_major_version(self) -> None:
        base_spec = get_fake_base_spec(info={"version": "2.0.1"})
        extra_spec: dict[str, Any] = {
            "info": {"version": "1.4.1"},
            "paths": {},
            "tags": [],
        }
        expected_version = "3.4.2"

        final_spec = merge_specs(
            base_spec=base_spec,
            specs={
                "spec1": extra_spec,
            },
        )

        assert final_spec["info"]["version"] == expected_version

    def test_it_adds_the_external_url(self) -> None:
        base_spec = get_fake_base_spec()
        extra_spec: dict[str, Any] = {
            "info": {},
            "paths": {},
            "tags": [],
        }
        expected_url = "https://my.supernice.url"

        final_spec = merge_specs(
            base_spec=base_spec,
            specs={
                "spec1": extra_spec,
            },
            external_server_url=expected_url,
        )

        assert final_spec["servers"][0]["url"] == expected_url
