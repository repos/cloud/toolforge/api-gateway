import copy
import http
import json
from contextlib import asynccontextmanager
from typing import Any, AsyncGenerator, AsyncIterator, cast
from unittest.mock import AsyncMock, MagicMock

import pytest
import pytest_asyncio
from httpx import AsyncByteStream, AsyncClient, Response

from app import api
from app.api import create_app
from app.settings import Settings, get_settings


# Just for the types
class MockedAsyncClient:
    send: AsyncMock
    get: AsyncMock
    post: AsyncMock
    patch: AsyncMock
    put: AsyncMock
    delete: AsyncMock


class StringStream(AsyncByteStream):
    def __init__(self, data: str):
        self.data = data

    async def __aiter__(self) -> AsyncIterator[bytes]:
        for line in self.data.splitlines():
            yield line.encode("utf-8")


@pytest_asyncio.fixture
async def multicall_httpx_client(
    monkeypatch: pytest.MonkeyPatch,
) -> dict[str, AsyncMock]:
    """
    Note that this might not work for some cases, so prefer the simple httpx_client for single calls.
    To use, you can overwrite the return_value/side_effect of each mock of each endpoint with:

    multicall_httpx_client["get"].return_value = Response(...)
    """
    methods = [
        "get",
        "post",
        "put",
        "delete",
        "patch",
        "head",
        "send",
    ]

    mocks = {method: AsyncMock(spec=getattr(AsyncClient, method)) for method in methods}

    # we have to create a new client for every call to `AsyncClient`
    @asynccontextmanager
    async def open_client(*args: Any, **kwargs: Any) -> AsyncIterator[AsyncClient]:
        async with AsyncClient() as httpx_client:
            for method, mock in mocks.items():
                monkeypatch.setattr(httpx_client, method, mock)

            yield httpx_client

    monkeypatch.setattr(api, "AsyncClient", open_client)
    return mocks


@pytest_asyncio.fixture
async def httpx_client(
    monkeypatch: pytest.MonkeyPatch,
) -> AsyncGenerator[MockedAsyncClient, None]:
    async with AsyncClient() as httpx_client:
        monkeypatch.setattr(api, "AsyncClient", lambda *_, **kwargs: httpx_client)

        for method in (
            "get",
            "post",
            "put",
            "delete",
            "patch",
            "head",
            "send",
        ):
            mock = AsyncMock(spec=getattr(httpx_client, method))
            monkeypatch.setattr(httpx_client, method, mock)

        yield cast(MockedAsyncClient, httpx_client)


@pytest.fixture
def app_settings(
    monkeypatch: pytest.MonkeyPatch,
) -> MagicMock:
    real_settings = get_settings()
    settings_mock = MagicMock(spec=real_settings, wraps=real_settings)
    monkeypatch.setattr(api, "get_settings", lambda: settings_mock)
    return settings_mock


@pytest_asyncio.fixture
async def app_client() -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(app=create_app(), base_url="http://localhost") as client:  # type: ignore[call-arg]
        yield client


class TestRoot:
    @pytest.mark.asyncio
    async def test_replies_ok(self, app_client: AsyncClient) -> None:
        response = await app_client.get("/")

        assert response.status_code == 200
        assert response.json() == {"message": "Welcome to the toolforge API 🦄"}


class TestProxiedRequest:
    @pytest.mark.asyncio
    async def test_returns_404_when_unknown_api(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={app_settings.original_uri_header: "/not_defined_api/something"},
        )

        assert response.status_code == http.HTTPStatus.NOT_FOUND
        assert response.json()["detail"] == "API 'not_defined_api' not found."

    @pytest.mark.asyncio
    async def test_returns_unauthorized_when_no_original_uri_header(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=toolforge",
            },
        )

        assert response.status_code == http.HTTPStatus.UNAUTHORIZED

    @pytest.mark.asyncio
    async def test_parses_ssl_header_and_sets_user_and_tool(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=toolforge",
                "X-Original-URI": "/silly_api/something",
            },
        )

        assert response.status_code == http.HTTPStatus.OK

        assert response.headers["x-toolforge-user"] == "sometool"
        assert response.headers["x-toolforge-tool"] == "sometool"

    @pytest.mark.asyncio
    async def test_parses_ssl_header_with_wrong_org_and_returns_forbidden(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=not-toolforge",
                app_settings.original_uri_header: "/silly_api/something",
            },
        )

        assert response.status_code == http.HTTPStatus.FORBIDDEN

    @pytest.mark.asyncio
    async def test_parses_bad_formatted_ssl_header_and_returns_unauthorized(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "Not well formatted",
                app_settings.original_uri_header: "/silly_api/something",
            },
        )

        assert response.status_code == http.HTTPStatus.UNAUTHORIZED

    @pytest.mark.asyncio
    async def test_parses_ssl_header_with_tool_in_path_same_as_sslcert_ok(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=toolforge",
                app_settings.original_uri_header: "/silly_api/something/tool/sometool/somethingelse",
            },
        )

        assert response.status_code == http.HTTPStatus.OK

        assert response.headers["x-toolforge-user"] == "sometool"
        assert response.headers["x-toolforge-tool"] == "sometool"

    @pytest.mark.asyncio
    async def test_ssl_header_with_tool_in_path_different_than_sslcert_is_forbidden(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}

        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=toolforge",
                app_settings.original_uri_header: "/silly_api/something/tool/notthesametool/somethingelse",
            },
        )

        assert response.status_code == http.HTTPStatus.FORBIDDEN

    @pytest.mark.asyncio
    async def test_ssl_header_with_missing_tool_in_path_with_tool_section_returns_unauthorized(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}
        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=sometool,O=toolforge",
                app_settings.original_uri_header: "/silly_api/something/tool/",
            },
        )

        assert response.status_code == http.HTTPStatus.UNAUTHORIZED

    @pytest.mark.asyncio
    async def test_ssl_header_with_superuser_is_allowed_for_tool(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}
        app_settings.superusers = ["superuser"]
        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=superuser,O=toolforge",
                app_settings.original_uri_header: "/silly_api/something/tool/anothertool",
            },
        )

        assert response.status_code == http.HTTPStatus.OK

    @pytest.mark.asyncio
    async def test_ssl_header_with_superuser_is_allowed_for_non_tool_path(
        self,
        app_client: AsyncClient,
        app_settings: Settings,
    ) -> None:
        app_settings.api_maps = {"silly_api": "https://some.url/to/api"}
        app_settings.superusers = ["superuser"]
        response = await app_client.get(
            "/auth",
            headers={
                app_settings.ssl_header: "CN=superuser,O=toolforge",
                app_settings.original_uri_header: "/silly_api/something/nontoolpath",
            },
        )

        assert response.status_code == http.HTTPStatus.OK


class TestOpenapi:
    @pytest.mark.asyncio
    async def test_returns_merged_openapis(
        self,
        app_client: AsyncClient,
        multicall_httpx_client: dict[str, AsyncMock],
        app_settings: Settings,
    ) -> None:
        # the tests on how to merge things are already done in test_merge.py
        # this is just a smoke test for the api endpoint
        openapi_docs1: dict[str, Any] = {
            "openapi": "3.0.1",
            "info": {"title": "Example API1", "version": "0.0.1"},
            "paths": {
                "/v1/endpoint": {
                    "get": {
                        "responses": {
                            "200": {
                                "content": {
                                    "application/json": {
                                        "schema": {
                                            "$ref": "#/components/schemas/Response"
                                        }
                                    }
                                },
                                "description": "Returns the list of builds",
                            }
                        }
                    }
                },
            },
            "components": {
                "schemas": {
                    "Response": {
                        "properties": {
                            "prop]": {"type": "string"},
                        },
                        "type": "object",
                    },
                }
            },
        }
        openapi_docs2 = copy.deepcopy(openapi_docs1)
        openapi_docs2["info"]["title"] = "Examlpe API2"

        multicall_httpx_client["get"].side_effect = [
            Response(
                status_code=http.HTTPStatus.OK,
                content=json.dumps(openapi_docs1),
                request=MagicMock(),
            ),
            Response(
                status_code=http.HTTPStatus.OK,
                content=json.dumps(openapi_docs2),
                request=MagicMock(),
            ),
        ]
        app_settings.api_maps = {
            "api1": "https://api1",
            "api2": "https://api2",
        }

        response = await app_client.get("/openapi.json")

        assert response.status_code == http.HTTPStatus.OK

        gotten_api_docs = response.json()

        assert "/api1/v1/endpoint" in gotten_api_docs["paths"]
        assert "/api2/v1/endpoint" in gotten_api_docs["paths"]
        assert (
            "#/components/schemas/api1Response"
            == gotten_api_docs["paths"]["/api1/v1/endpoint"]["get"]["responses"]["200"][
                "content"
            ]["application/json"]["schema"]["$ref"]
        )
        assert (
            "#/components/schemas/api2Response"
            == gotten_api_docs["paths"]["/api2/v1/endpoint"]["get"]["responses"]["200"][
                "content"
            ]["application/json"]["schema"]["$ref"]
        )
        assert "api1Response" in gotten_api_docs["components"]["schemas"]
        assert "api2Response" in gotten_api_docs["components"]["schemas"]
