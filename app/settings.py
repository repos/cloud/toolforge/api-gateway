from functools import lru_cache
from pathlib import Path

from pydantic import AnyHttpUrl
from pydantic_settings import BaseSettings, SettingsConfigDict


class OpenapiDocsSettings(BaseSettings):
    builds: bool = True
    jobs: bool = True
    envvars: bool = True
    components: bool = True
    external_api_url: AnyHttpUrl = AnyHttpUrl("https://api.svc.toolforge.org")


class Settings(BaseSettings):
    # allows using envvars like 'APP_API_CLIENT_KEY'
    model_config = SettingsConfigDict(env_prefix="APP_", env_ignore_empty=True)
    log_level: str = "info"

    # the following three settings are used by uvicorn mostly
    port: int = 8000
    address: str = "127.0.0.1"

    ssl_header: str = "ssl-client-subject-dn"
    original_uri_header: str = "x-original-uri"

    backend_timeout_s: int = 10
    backend_logs_timeout_s: int = 60 * 10

    client_ca_path: Path = Path("/etc/nginx/tls/backend/ca.crt")
    client_cert_path: Path = Path("/etc/nginx/tls/backend/tls.crt")
    client_key_path: Path = Path("/etc/nginx/tls/backend/tls.key")
    api_maps: dict[str, str] = {
        "builds": "https://builds-api.builds-api.svc.cluster.local:8443",
        "jobs": "https://jobs-api.jobs-api.svc.cluster.local:8443",
        "envvars": "https://envvars-api.envvars-api.svc.cluster.local:8443",
        "components": "https://components-api.components-api.svc.cluster.local:8443",
    }
    openapi_docs: OpenapiDocsSettings = OpenapiDocsSettings()
    # these users will be able to do anything on any tool, passed down to APIs in the user header
    # note that this are the CN in the ssl certificates
    superusers: list[str] = []


@lru_cache(maxsize=1)
def get_settings() -> Settings:
    return Settings()
