from typing import Any


def rebase_refs(spec: dict[str, Any], old_path: str, new_path: str) -> None:
    for key, value in spec.items():
        if key == "$ref" or (
            isinstance(value, str) and value.startswith(f"#{old_path}")
        ):
            new_value = value.replace(old_path, new_path)
            spec[key] = new_value

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    rebase_refs(spec=item, old_path=old_path, new_path=new_path)

        else:
            if isinstance(value, dict):
                rebase_refs(spec=value, old_path=old_path, new_path=new_path)


def rebase_api_paths(spec: dict[str, Any], base_url: str) -> None:
    rebased_paths = {
        f"{base_url}{old_url}": data
        for old_url, data in spec["paths"].items()
        if old_url != "/openapi.json"
    }
    spec["paths"] = rebased_paths


def prefix_operation_ids(spec: dict[str, Any], prefix: str) -> None:
    for path in spec.get("paths", {}).values():
        for method_spec in path.values():
            if "operationId" in method_spec:
                method_spec["operationId"] = f"{prefix}_{method_spec['operationId']}"


def add_tags(spec: dict[str, Any], tag_name: str) -> None:
    for path in spec.get("paths", {}).values():
        for method_spec in path.values():
            method_spec["tags"] = [tag_name]


def merge_versions(version1: str, version2: str) -> str:
    return ".".join(
        str(int(chunk1) + int(chunk2))
        for chunk1, chunk2 in zip(version1.split("."), version2.split("."))
    )


def merge_specs(
    base_spec: dict[str, Any],
    specs: dict[str, dict[str, Any]],
    external_server_url: str = "no_url",
) -> dict[str, Any]:
    merged_paths = base_spec.get("paths", {}).copy()
    merged_schemas = base_spec.get("components", {}).get("schemas", {}).copy()
    merged_responses = base_spec.get("components", {}).get("responses", {}).copy()

    base_spec.setdefault("tags", [])
    base_spec.setdefault("info", {})

    for spec_name, spec in specs.items():
        rebase_api_paths(spec=spec, base_url=f"/{spec_name}")
        for subpath in ("schemas", "responses", "parameters"):
            rebase_refs(
                spec=spec,
                old_path=f"/components/{subpath}/",
                new_path=f"/components/{subpath}/{spec_name}",
            )
        prefix_operation_ids(spec=spec, prefix=spec_name)
        add_tags(spec=spec, tag_name=spec_name.capitalize())
        new_tag = {"name": spec_name.capitalize()}
        if new_tag not in base_spec["tags"]:
            base_spec["tags"].append(new_tag)

        merged_paths.update(spec.get("paths", {}))
        merged_schemas.update(
            {
                f"{spec_name}{component_name}": component_value
                for component_name, component_value in spec.get("components", {})
                .get("schemas", {})
                .items()
            }
        )
        merged_responses.update(
            {
                f"{spec_name}{component_name}": component_value
                for component_name, component_value in spec.get("components", {})
                .get("responses", {})
                .items()
            }
        )
        base_spec["info"]["version"] = merge_versions(
            version1=base_spec.get("info", {}).get("version", "0.0.0"),
            version2=spec.get("info", {}).get("version", "0.0.0"),
        )

    base_spec["paths"] = merged_paths
    base_spec["components"]["schemas"] = merged_schemas
    base_spec["components"]["responses"] = merged_responses
    base_spec["servers"] = [{"url": external_server_url}]

    return base_spec
