import http
import logging
from copy import deepcopy
from functools import lru_cache
from pathlib import Path
from typing import Any, cast

import yaml
from fastapi import APIRouter, FastAPI, HTTPException
from httpx import AsyncClient, HTTPStatusError, RequestError
from starlette.datastructures import MutableHeaders
from starlette.requests import Request
from starlette.responses import Response

from app.merge import merge_specs

from .auth import Identification
from .settings import Settings, get_settings

router = APIRouter()


LOGGER = logging.getLogger(__name__)


@lru_cache(maxsize=1)
def get_base_spec() -> dict[str, Any]:
    curdir = Path(__file__).parent
    # technically yaml can return a list, but our base_openapi is a dict
    return cast(
        dict[str, Any],
        yaml.safe_load((curdir / "base_openapi.yaml").open("r", encoding="utf8")),
    )


async def get_openapi_spec(url: str, settings: Settings) -> dict[str, Any]:
    try:
        async with AsyncClient(
            verify=str(settings.client_ca_path),
            cert=(
                str(settings.client_cert_path),
                str(settings.client_key_path),
            ),
        ) as client:
            response = await client.get(url)
            response.raise_for_status()
            openapi_spec = response.json()

        if not isinstance(openapi_spec, dict):
            raise HTTPException(
                status_code=http.HTTPStatus.INTERNAL_SERVER_ERROR,
                detail=f"Backend API error while fetching {url}. Malformed api spec (expecting a json dict).",
            )

        return openapi_spec

    except HTTPStatusError as error:
        raise HTTPException(
            status_code=error.response.status_code,
            detail=f"Backend API error while fetching {url}: {error}.",
        ) from error

    except RequestError as error:
        raise HTTPException(
            status_code=500,
            detail=f"Connection error with backend API while fetching url {url}: {error}.",
        ) from error


@router.get("/")
def read_root() -> dict[str, str]:
    return {"message": "Welcome to the toolforge API 🦄"}


@router.get("/healthz")
def healthz() -> dict[str, str]:
    return {"status": "ok"}


@router.get("/openapi.json")
async def get_openapi() -> dict[str, Any]:
    # using Annotated type annotations makes it really hard to mock
    settings = get_settings()
    openapi_specs: dict[str, dict[str, Any]] = {}
    for api_path, api_url in settings.api_maps.items():
        should_show = getattr(settings.openapi_docs, api_path, True)
        if not should_show:
            continue
        if api_path.startswith("/"):
            api_name = api_path[1:]
        else:
            api_name = api_path
        openapi_specs[api_name] = await get_openapi_spec(
            url=f"{api_url}/openapi.json", settings=settings
        )

    # the cache gives you the same instance of the dict for every call
    # to be able to change it without affecting other callers
    # we have to make a copy of it
    base_spec = deepcopy(get_base_spec())

    full_spec = merge_specs(
        base_spec=base_spec,
        specs=openapi_specs,
        external_server_url=settings.openapi_docs.external_api_url.unicode_string(),
    )

    return full_spec


def sanitize_headers(headers: MutableHeaders) -> None:
    for header_to_drop in headers.keys():
        if header_to_drop.lower().startswith("x-toolforge"):
            del headers[header_to_drop]


@router.get("/auth")
async def auth(
    request: Request,
    response: Response,
) -> Identification:
    LOGGER.debug("New auth request")
    LOGGER.debug("Incoming headers: %r", request.headers)
    # using Annotated type annotations makes it really hard to mock
    settings = get_settings()

    try:
        requested_path = request.headers[settings.original_uri_header]
    except KeyError as error:
        LOGGER.debug(
            "Unauthorized, missing %s header in auth request",
            settings.original_uri_header,
        )
        raise HTTPException(
            status_code=http.HTTPStatus.UNAUTHORIZED,
            detail=f"Missing {settings.original_uri_header} header in the auth request",
        ) from error

    # requested_path comes with the first `/` included
    api_name = requested_path.split("/", 2)[1]
    api_url = settings.api_maps.get(api_name)
    if not api_url:
        LOGGER.debug("Not found, API %s not in the config", api_name)
        raise HTTPException(
            status_code=http.HTTPStatus.NOT_FOUND, detail=f"API '{api_name}' not found."
        )

    identification = await Identification.from_request(
        request=request, settings=settings
    )
    LOGGER.debug("Got ident: %r", identification)

    new_headers = request.headers.mutablecopy()
    sanitize_headers(headers=new_headers)

    identification.add_to_headers(new_headers)
    LOGGER.debug("Reply headers: %r", new_headers)

    response.headers.update(new_headers)

    return identification


def create_app() -> FastAPI:
    settings = get_settings()
    try:
        level = getattr(logging, settings.log_level.upper())
    except AttributeError:
        level = logging.INFO

    logging.basicConfig(level=level)
    LOGGER.debug("Got settings: %r", settings)

    app = FastAPI(openapi_url=None)
    app.include_router(router)
    return app
