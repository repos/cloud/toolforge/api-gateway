import http
import logging
import urllib
import urllib.parse

from cryptography import x509
from fastapi import HTTPException
from pydantic import BaseModel
from starlette.datastructures import MutableHeaders
from starlette.requests import Request

from .settings import Settings

LOGGER = logging.getLogger(__name__)


class Identification(BaseModel):
    user: str = ""
    tool: str = ""

    def add_to_headers(self, headers: MutableHeaders) -> None:
        if self.user:
            headers["X-Toolforge-User"] = self.user

        if self.tool:
            headers["X-Toolforge-Tool"] = self.tool

    def authenticate(self, request: Request, settings: Settings) -> None:
        url_path = request.headers[settings.original_uri_header]
        if settings.ssl_header in request.headers:
            # only tools and services are allowed to do ssl auth, for tools the user and tool must be the same
            # for services the tool is the one in the path
            self.user = self.tool = self._get_tool_from_ssl_headers(
                ssl_header=request.headers[settings.ssl_header]
            )

            # for superusers, the tool comes from the path
            if self.user in settings.superusers:
                tool_from_path = self._get_tool_from_path(url_path=url_path)
                if tool_from_path is not None:
                    self.tool = tool_from_path

                LOGGER.debug(
                    "Found superuser certificate (%s), got tool from path %s",
                    self.user,
                    tool_from_path,
                )

        if self._is_deploy_url_with_token(url_path=url_path):
            LOGGER.debug(
                "Got deploy url with token, delegating auth to the components-api"
            )
            return

        self._check_tool_matches_path(url_path=url_path)
        # Add here other ways of identification

    def _get_tool_from_ssl_headers(self, ssl_header: str) -> str:
        """
        When authenticating with ssl headers, we expect that the certificate is the tool certificate, this means that the tool and the user is the same.
        """
        error_detail = f"Failed to parse certificate name '{ssl_header}'"
        auth_error = HTTPException(
            status_code=http.HTTPStatus.UNAUTHORIZED,
            detail=error_detail,
        )

        # we are expecting something like 'CN=user,O=Toolforge'
        try:
            name = x509.Name.from_rfc4514_string(ssl_header)
        except Exception as error:
            LOGGER.debug("%s: %s", error_detail, str(error))
            raise auth_error from error

        cn = name.get_attributes_for_oid(x509.NameOID.COMMON_NAME)
        organizations = [
            attr.value
            for attr in name.get_attributes_for_oid(x509.NameOID.ORGANIZATION_NAME)
        ]

        if len(cn) != 1:
            raise auth_error

        if organizations != ["toolforge"]:
            message = (
                "This certificate can't access the Toolforge APIs. "
                "Double check you're logged in to the correct account? "
                f"(got {organizations})"
            )
            LOGGER.debug(message)
            raise HTTPException(
                status_code=http.HTTPStatus.FORBIDDEN,
                detail=message,
            )

        common_name = cn[0].value
        if isinstance(common_name, bytes):
            return common_name.decode("utf-8")

        return str(common_name)

    @staticmethod
    def _get_tool_from_path(url_path: str) -> str | None:
        path_parts = url_path.split("/")
        if "tool" not in path_parts:
            return None

        try:
            tool_from_path = path_parts[path_parts.index("tool") + 1]
        except IndexError:
            tool_from_path = ""

        return tool_from_path

    def _is_deploy_url_with_token(self, url_path: str) -> bool:
        parsed_url = urllib.parse.urlparse(url_path)
        is_deploy_url = "deployment" in parsed_url.path.split("/")
        params = urllib.parse.parse_qs(parsed_url.query)
        maybe_token = params.get("token", [])
        LOGGER.debug(
            "Got url (%s, %s a deploy url) with token (%s)",
            url_path,
            "is" if is_deploy_url else "is not",
            maybe_token,
        )
        return bool(is_deploy_url and maybe_token)

    def _check_tool_matches_path(self, url_path: str) -> None:
        tool_from_path = self._get_tool_from_path(url_path=url_path)
        if tool_from_path is None:
            LOGGER.debug("Path (%s) has no tool prefix, skipping", url_path)
            return

        if tool_from_path == "":
            message = f"Missing actual tool name in path ({url_path})"
            LOGGER.debug(message)
            raise HTTPException(
                status_code=http.HTTPStatus.UNAUTHORIZED,
                detail=message,
            )

        if self.tool != tool_from_path:
            message = (
                f"User {self.user} is not authorized to act on tool {tool_from_path}"
            )
            LOGGER.debug(message)
            raise HTTPException(
                status_code=http.HTTPStatus.FORBIDDEN,
                detail=message,
            )

    @classmethod
    async def from_request(
        cls, request: Request, settings: Settings
    ) -> "Identification":
        ident = cls()
        ident.authenticate(request=request, settings=settings)
        return ident
